///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 10x - Collection Class Evaluator
///
/// @file eval.cpp
/// @version 1.0
///
/// Evaluate a number of Standard C++ Collection classes for performance
///
/// @author Kayla Valera <kvalera@hawaii.edu>
/// @brief  Lab 10x - Collection Class Evaluator - EE 205 - Spr 2021
/// @date   @todo 14_MAY_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_map>
#include <unordered_set>
#include <cstring>
#include <string>
#include <cmath>
#include <x86intrin.h>
#include <limits.h>

using namespace std;

typedef unsigned long ticks_t;

constexpr unsigned int TESTS              = 5;    // The number of elements in TEST_SIZE[]
constexpr unsigned int TEST_SIZE[TESTS]   = { 10, 100, 1000, 10000, 100000 };
constexpr unsigned int THROWAWAY_RUNS     = 2;    // Unused runs that initialize memory
constexpr unsigned int TEST_RUNS          = 10;   // Measured test runs
constexpr unsigned int SEARCH_TESTS       = 100;  // Number of times we try searching the data structure
constexpr unsigned int CALIBRATE_OVERHEAD = 1000; // Number of times to calibrate the test overhead
// #define PRINT_PROGRESS

// Inline assembly routine that returns high percision monotonic clock
// ticks from the CPU
#define MARK_TIME(ticks)  asm inline volatile (               \
                          "RDTSCP\n\t"                        \
                          "SHL $32,   %%rdx\n\t"              \
                          "OR  %%rax, %%rdx\n\t"              \
                          "MOV %%rdx, %0\n\t"                 \
                          : "=r" (ticks)                      \
                          :                                   \
                          : "%rax", "%rcx", "%rdx")           ;

long TEST_OVERHEAD = LONG_MAX;

// Compute the number of ticks between start and end, subtracting the overhead.
// If it's negative, then it's 0.
#define DIFF_TIME(start, end) ((end-start-TEST_OVERHEAD > 0) ? (end-start-TEST_OVERHEAD) : 0)



/// This abstract test class is a template for concrete classes that can
///// actually do tests.
class ABSTRACT_TEST_CLASS {
protected:
 static long start, end;
 static int  insert_value;
 static int  search_value;
 
public:
  virtual inline void    initDataStructure()  __attribute__((always_inline)) = 0;
  virtual inline ticks_t testInsert()         __attribute__((always_inline)) = 0;
  virtual inline ticks_t clearDataStructure() __attribute__((always_inline)) = 0;
  virtual inline ticks_t testSearch()         __attribute__((always_inline)) = 0;
};

long ABSTRACT_TEST_CLASS::start;
long ABSTRACT_TEST_CLASS::end;
int  ABSTRACT_TEST_CLASS::insert_value;
int  ABSTRACT_TEST_CLASS::search_value;

//////////////////////////////////  vector  //////////////////////////////////

class TestVector : public ABSTRACT_TEST_CLASS {
	private:
	static vector<long> container;
	public:
	virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

	virtual inline ticks_t testInsert() __attribute__((always_inline)) {
	auto endIterator = container.end();
	insert_value = rand();

	MARK_TIME( start );

//operation//

	container.insert( endIterator, insert_value );
///////////////////////////////////////////////////////////////////
MARK_TIME( end );

      return ( DIFF_TIME( start, end ) );
         }

virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
		     
	MARK_TIME( start );

	container.clear();

	MARK_TIME( end );

	return ( DIFF_TIME( start, end ) );
}

 virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
	 search_value = rand();

	 MARK_TIME( start );

	for( auto i : container ) {
	if( i == search_value )
	break;
}

MARK_TIME( end );

      return ( DIFF_TIME( start, end ) );
         }

};
/////////////////////////////////// list  ///////////////////////////////////

class TestList : public ABSTRACT_TEST_CLASS {
private:
	static list<long> container;

public:

virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

virtual inline ticks_t testInsert() __attribute__((always_inline)) {

	auto endIterator = container.end();
				     
	insert_value = rand();

	MARK_TIME( start );
///////operation///////////
      container.insert( endIterator, insert_value );
///////////////////////////
  MARK_TIME( end );

        return ( DIFF_TIME( start, end ) );
	   }

 virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {

	MARK_TIME( start );

	container.clear();

	MARK_TIME( end );

	return ( DIFF_TIME( start, end ) );
}

virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
	search_value = rand();

	MARK_TIME( start );

	for( auto i : container ) {
		if( i == search_value )
			break;
	}
	
	MARK_TIME( end );

	return ( DIFF_TIME( start, end ) );
}

};

list<long> TestList::container;


///////////////////////////////////  set  ///////////////////////////////////
class TestSet : public ABSTRACT_TEST_CLASS {
	private:
		static set<long> container;

	public:
		virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

		virtual inline ticks_t testInsert() __attribute__((always_inline)) {
		auto endIterator = container.end();
		insert_value = rand();

		MARK_TIME( start );
////////operation//////
      container.insert( endIterator, insert_value );

//////////////////
	 MARK_TIME( end );

       		return ( DIFF_TIME( start, end ) );
 }

	virtual inline ticks_t clearDataStructure()   __attribute__((always_inline)) {
			         
	MARK_TIME( start );
	
	container.clear();

	MARK_TIME( end );

	return ( DIFF_TIME( start, end ) );
}

	virtual inline ticks_t testSearch()   __attribute__((always_inline)) {
		search_value = rand();

	MARK_TIME( start );

	for( auto i : container ) {
		if( i == search_value )
			break;
}

	MARK_TIME( end );

	return ( DIFF_TIME( start, end ) );
}

};

set<long> TestSet::container;

//////////////////////////////////  map  //////////////////////////////////
class TestMap : public ABSTRACT_TEST_CLASS {
	private:
		static map<long, long> container;

	public:
		virtual inline void initDataStructure() __attribute__((always_inline)) { container.clear(); }

		virtual inline ticks_t testInsert() __attribute__((always_inline)) {
 insert_value = rand();

       MARK_TIME( start );

///////////////diff operation//////////
      container.insert(pair<long, long>( insert_value, insert_value) );

//////////////////////////////////////


///////////////////////////////////  main  ///////////////////////////////////
int main() {
	cout << "Welcome to the Gnu C++ Collection Class Evaluator" << endl;

	static long start, end;
	for( unsigned int i = 0 ; i < CALIBRATE_OVERHEAD ; i++ ) {
		MARK_TIME( start );
		MARK_TIME( end );
		TEST_OVERHEAD = min( TEST_OVERHEAD, (end - start) );
	}

	cout << "Approximate test overhead is: " << TEST_OVERHEAD << endl;

	// Implement your Collection Class Evaluator here

} // main()
